#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
   int vars;
   int i;
   int x;
   int l;
   int prepositions;
   char * line;
   int length;
   printf("how many variables do you want\n");
   scanf("%d" , &vars);

   printf("how many prepositions do you need\n");
   scanf("%d" , &prepositions);

   for(i = 0; i < (2^vars)+2; i++){
     if(i == 2){
      for( x =0; x<vars*2 + 1;x++){
          strcat(line, "-");
          length+= 1;
      }
      printf("%s\n" , line);
     }else{
        for(l = 0, l < vars; i++;){
            if(vars == l){
                strcat(line, "| |");
                length+=3;
            }else{
             strcat(line, "| ");
             length+=2;
            }
        }
        printf("%s" , line);
     }
     bzero(line,length);
   }
    return 0;
}

